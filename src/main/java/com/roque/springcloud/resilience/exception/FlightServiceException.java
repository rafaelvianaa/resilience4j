package com.roque.springcloud.resilience.exception;

public class FlightServiceException extends RuntimeException {
	public FlightServiceException(String message) {
		super(message);
	}
}
