package com.roque.springcloud.resilience.failure;

public interface PotentialFailure {
    void occur() ;
}