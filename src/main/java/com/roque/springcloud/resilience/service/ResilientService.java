package com.roque.springcloud.resilience.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Component
public class ResilientService implements Service {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(ResilientService.class);

	private RestTemplate restTemplate = new RestTemplate();

	private static final String ENDPOINT_OK = "http://localhost:8083/orders/ok";
	private static final String ENDPOINT_DOWN = "http://localhost:8083/orders/down";

	@CircuitBreaker(name = "teste")
	public void chamadaInternaSucesso() {
		LOGGER.info("Chamada interna sucesso");
	}

	@CircuitBreaker(name = "teste", fallbackMethod = "chamadaInternaFalhaFallback")
	public void chamadaInternaFalha() {
		throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "This is a remote exception");
	}
	
	public void chamadaInternaFalhaFallback(Throwable t) {
		System.out.println("Chamda interna fora");
	}

	@CircuitBreaker(name = "teste" )
	public void chamadaExternaSucesso() {
		ResponseEntity<String> response
		= restTemplate.getForEntity(ENDPOINT_OK, String.class);
	}
	
	@CircuitBreaker(name = "teste" , fallbackMethod = "chamadaExternaFalhaFallback")
	public void chamadaExternaFalha() {
		ResponseEntity<String> response
		= restTemplate.getForEntity(ENDPOINT_DOWN, String.class);
	}
	
	public void chamadaExternaFalhaFallback(Throwable t) {
		System.out.println("Serviço externo fora");
	}




}
