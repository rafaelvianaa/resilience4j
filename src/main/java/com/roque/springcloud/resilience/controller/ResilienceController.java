package com.roque.springcloud.resilience.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.roque.springcloud.resilience.service.ResilientService;

@RestController
@RequestMapping("/resilience")
public class ResilienceController {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(ResilienceController.class);

	@Autowired
	private ResilientService service;

	@GetMapping("/internalSuccess")
	public void internalSuccess()   {
		service.chamadaInternaSucesso();
	}
	
	@GetMapping("/internalFailure")
	public void internal()   {
		try {
			
		}catch(Exception e) {
			LOGGER.error("Erro chamada interna" , e);
		}
		service.chamadaInternaFalha();
	}
	
	@GetMapping("/externalSuccess")
	public void externalSuccess()   {
		service.chamadaExternaSucesso();
	}
	
	@GetMapping("/externalFailure")
	public void externalFailure()   {
		service.chamadaExternaFalha();
	}
	
	

}
